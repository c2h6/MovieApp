package de.etadar.movieapp;

public class Movie {

    private final String posterURL;
    private final String title;
    private final String date;
    private final String voteAvg;
    private final String plot;

    Movie(String title, String date, String posterURL, String voteAvg, String plot) {
        this.posterURL=posterURL;
        this.title=title;
        this.date=date;
        this.voteAvg=voteAvg;
        this.plot=plot;
    }
    public String getPosterUrl() {
        return posterURL;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getVoteAvg() { return voteAvg; }

    public String getPlot() { return plot; }

}
