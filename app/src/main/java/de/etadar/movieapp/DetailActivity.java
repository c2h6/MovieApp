package de.etadar.movieapp;

import static de.etadar.movieapp.Utils.movies; //TODO: Does this make sense?

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Bundle extras = getIntent().getExtras();
        int index = extras.getInt("index");
        Movie movie = movies.get(index);

        TextView titleTV = findViewById(R.id.tv_title);
        titleTV.setText(movie.getTitle());

        ImageView thumbIV = findViewById(R.id.iv_thumb);
        Picasso.get().load("http://image.tmdb.org/t/p/w500"+movie.getPosterUrl()).into(thumbIV);
        // .fit().centerInside() does not work - why?

        TextView plotTV = findViewById(R.id.tv_plot);
        plotTV.setText(movie.getPlot());

        TextView ratingTV = findViewById(R.id.tv_rating);
        ratingTV.setText("Average Rating: "+movie.getVoteAvg());

        TextView dateTV = findViewById(R.id.tv_date);
        dateTV.setText("Release Date: "+movie.getDate());
    }
}
