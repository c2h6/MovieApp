package de.etadar.movieapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieAdapter extends ArrayAdapter<Movie> {
    private final Context context;

    MovieAdapter(Context context, List<Movie> movies) {
        super(context, 0, movies);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Movie movie = getItem(position);

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.movie_tile, null);

            final ImageView posterIV = convertView.findViewById(R.id.iv_poster);

            final ViewHolder viewHolder = new ViewHolder(posterIV);
            convertView.setTag(viewHolder);
        }

        final ViewHolder viewHolder = (ViewHolder)convertView.getTag();

        String PosterURL = "http://image.tmdb.org/t/p/w500"+movie.getPosterUrl();
//        int width = context.getResources().getDisplayMetrics().widthPixels;
        Picasso.get().load(PosterURL).placeholder(R.drawable.ic_stub).into(viewHolder.poster);

        return convertView;
    }

    private class ViewHolder {
        private final ImageView poster;

        ViewHolder(ImageView poster) {
            this.poster = poster;
        }
    }
}
