package de.etadar.movieapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static de.etadar.movieapp.Utils.movies;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    public static final String TMDB_API_KEY="ad2f235d6d2b59f42ac7209e29ed909e";

    private MovieAdapter movieAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GridView gridView = findViewById(R.id.gridview);

        String url = "http://api.themoviedb.org/3/movie/popular?api_key="+TMDB_API_KEY;

        movieAdapter = new MovieAdapter(this, movies);
        gridView.setAdapter(movieAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                Intent myIntent = new Intent(MainActivity.this, DetailActivity.class);
                myIntent.putExtra("index", position);
                startActivity(myIntent);
            }
        });

        Request request = new Request.Builder()
                .url(url)
                .build();

        fetchJSON(request);

        //TODO: Settings menu for sort order
    }

    private void parseJSON (String response) throws JSONException {

//        Log.i(TAG, "parseJSON: Response: "+response);
//        movies.clear();
        movieAdapter.clear();
        JSONObject json = new JSONObject(response);

        JSONArray results = json.getJSONArray("results");

        for(int i =0; i < results.length(); i++) {

            JSONObject result = results.getJSONObject(i);
            String title = result.getString("title");
            String date = result.getString("release_date");
            String imageUrl = result.getString("poster_path");
            String vote = result.getString("vote_average");
            String plot = result.getString("overview");

            Movie movie = new Movie(title, date, imageUrl, vote, plot);
            movieAdapter.add(movie);
//            Log.i(TAG, "parseJSON: Found movie Title: "+title);
        }

    }

    void fetchJSON (Request request) {
//TODO: check for internet connectivity
        OkHttpClient client = new OkHttpClient();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            parseJSON(myResponse);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        movieAdapter.notifyDataSetChanged();
                        //TODO: On first run, does not display anything
                    }
                });

            }
        });
    }

    // baby book tutorial: https://www.raywenderlich.com/127544/android-gridview-getting-started
}
